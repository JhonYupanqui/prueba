<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Modulo de Empleados</title>
  <link rel="stylesheet" href="{{ asset('/css/app.css') }}">
  
</head>
<body>
  <header>
    <h1>Lista de Empleados</h1>
  </header>
  <main class="container-fluid" id="appVue">

    <jy-empleado-page></jy-empleado-page>
     
  </main>
  <footer>
    
  </footer>
</body>

<script src="{{ asset('/js/app.js') }}"></script>

</html>