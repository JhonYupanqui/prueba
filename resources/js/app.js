require('./bootstrap');


import axios from 'axios'

const instanceRoot = axios.create({
  baseURL: 'http://prueba.test/api'
})

instanceRoot.defaults.headers.common['Content-Type'] = 'application/json'
instanceRoot.defaults.headers.common['Accept'] = 'application/json'
 
 

import EmpleadoPage from './pages/EmpleadoPage.vue';

const app  = new Vue({
  el: '#appVue',
  components: {
    'jy-empleado-page':EmpleadoPage
  },
  data: {
   // titulo: "Empleados"
  }
})