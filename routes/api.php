<?php

use Illuminate\Support\Facades\Route;


Route::get('/empleado/lista','EmpleadoRestController@lista')->name('empleado.lista');
Route::get('/empleado/{empleado}/show','EmpleadoRestController@show')->name('empleado.lista');
Route::post('/empleado/store','EmpleadoRestController@store')->name('empleado.store');
Route::post('/empleado/{empleado}/update','EmpleadoRestController@update')->name('empleado.update');
Route::post('/empleado/{empleado}/delete','EmpleadoRestController@delete')->name('empleado.delete');

