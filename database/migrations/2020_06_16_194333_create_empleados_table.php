<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmpleadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empleados', function (Blueprint $table) {
            $table->id();
            $table->string('nombres');
            $table->string('apellidos');
            $table->date('fechaNacimiento');
            $table->date('fechaIngreso');
            $table->string('afp',12);
            $table->bigInteger('cargo_id')->unsigned();
            $table->double('sueldo',2);
       
            $table->timestamps();
            
            $table->foreign('cargo_id')->references('id')->on('cargos');
           
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empleados');
    }
}
