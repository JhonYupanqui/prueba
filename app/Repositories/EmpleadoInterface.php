<?php 

namespace App\Repositories;

use App\Repositories\Base\BaseRepoInterface; 

interface EmpleadoInterface
{

  public function all(array $data);

  public function findById(array $data);

  public function create(array $data);

  public function update(array $data, $id);

  public function delete($id);
 
 
}