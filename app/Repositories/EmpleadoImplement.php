<?php 

namespace App\Repositories;


use App\Empleado;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class EmpleadoImplement implements EmpleadoInterface
{

  protected $model;

  public function __construct(Empleado $empleado){
    $this->model = $empleado;
  }
  
  public function all(array $parameters = []){

    $dataProducto = $this->model->get();
  
    return $dataProducto; 
  }

  public function findById($id){
       
    if (null == $empleado = $this->model->find($id)) {
      throw new ModelNotFoundException(); 
    }
    
    return $empleado;
      
  }

  public function create(array $data){
  
    $dataCreated = $this->model->create($data);
   
    return $dataCreated;

  }

  public function update(array $data, $id){
    
    if (null == $empleado = $this->model->find($id)) {
      throw new ModelNotFoundException(); 
    }
 
   
    $empleado->update($data);
 
    return $empleado;
  }

  public function delete($id){

    if (null == $empleado = $this->model->find($id)) {
      throw new ModelNotFoundException(); 
    }
 
    $empleado->destroy($id);
 
    return $empleado;
  }

  
 

}