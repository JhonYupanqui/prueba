<?php

namespace App\Providers;

use App\Repositories\EmpleadoImplement;
use App\Repositories\EmpleadoInterface;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
      $this->app->bind(EmpleadoInterface::class, EmpleadoImplement::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
      //
    }
}
