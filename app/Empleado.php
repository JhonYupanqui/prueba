<?php

namespace App;

use App\Cargo;
use Illuminate\Database\Eloquent\Model;
use App\Http\Resources\EmpleadoCollection;
use App\Http\Resources\Empleado as EmpleadoResource;

class Empleado extends Model
{
 
  protected $table = 'empleados';
 
  public $apiResource = EmpleadoResource::class;
  public $apiResourceCollection = EmpleadoCollection::class;

  //Empleado

  protected $fillable = [
    'nombres',
    'apellidos',
    'fechaNacimiento',
    'fechaIngreso',
    'afp',
    'cargo_id',
    'sueldo'
  ];
 
  public function cargo(){
    return $this->belongsTo(Cargo::class);
  }
 
 
}
