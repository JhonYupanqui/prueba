<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class EmpleadoViewsController extends Controller
{
  public function view()
  {
    return view('administrador.empleado.index');
  }
}
