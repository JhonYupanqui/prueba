<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\EmpleadoInterface;

class EmpleadoRestController extends Controller
{

  private $empleadoRepoItf;

  public function __construct(EmpleadoInterface $empleadoRepoInterface){
      $this->empleadoRepoItf = $empleadoRepoInterface;
  }

  public function lista(Request $request)
  {
    
    $empleados = $this->empleadoRepoItf->all($request->all());
  
    return $this->showCollect($empleados);
 
  }

  public function show($empleado)
  {
    $empleado = $this->empleadoRepoItf->findById($empleado);
   
    return $this->showModel($empleado); 

  }

  public function store(Request $request)
  { 
     
    $detailsEmpleado = $request->only([ 'nombres',
                                        'apellidos',
                                        'fechaNacimiento',
                                        'fechaIngreso',
                                        'afp',
                                        'cargo_id',
                                        'sueldo'
                                    ]);
    
    $empleado = $this->empleadoRepoItf->create($detailsEmpleado);
      
   
    return $this->showModel($empleado); 
         

  }

  public function update(Request $request, $empleado)
  { 
    $detailsEmpleado = $request->only(['nombres',
                                      'apellidos',
                                      'fechaNacimiento',
                                      'fechaIngreso',
                                      'afp',
                                      'cargo_id',
                                      'sueldo'
                                    ]);
   
    $empleado = $this->empleadoRepoItf->update($detailsEmpleado, $empleado); 
 
    return $this->showModel($empleado);
       
  }

  public function delete($empleado)
  {
 
    $empleadoo = $this->empleadoRepoItf->delete($empleado); 
 
    return $this->showModel($empleadoo);

  }
 
    
}
