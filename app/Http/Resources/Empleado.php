<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Empleado extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
      return [
        'identificador' => $this->id,
        'name' => $this->nombres,
        'apellidos' => $this->apellidos,
        'fechaNacimiento' => $this->fechaNacimiento,
        'fechaIngreso' => $this->fechaIngreso,
        'afp' => $this->afp,
        'cargo_id' => $this->cargo_id,
        'cargo' => $this->cargo->nombre,
        'sueldo' => $this->sueldo
    ]; 
    }
}
