<?php 

namespace App\Traits;

use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;
use Illuminate\Pagination\LengthAwarePaginator;
  
trait Responser
{

  #PRIVATE
    private function successResponse($message,$code){
      return response()->json(["error"=>false,"dataResponse"=>$message],$code);
    }

    private function errorResponse($message,$code){
        return response()->json(["error"=>true,"message"=>$message,"code"=>$code],$code);
    }

    private function dataResponse($data,$code){
        return response()->json(["error"=>false,"dataResponse"=>$data],$code);
    }
 
  #END PRIVATE

  
  #PROTECTED

    protected function successMessage($message, $code = 200){
      return $this->successResponse($message,$code);
    }

    protected function errorMessage($message,$code = 500){
      return $this->errorResponse($message,$code);
    }
 
    
    protected function showModel(Model $instance, $code = 200){
      
      if($instance->get()->isEmpty()){ //SI esta vacio el modelo enviado
        return $this->dataResponse([], $code);
      } 

      $apiResource = $instance->apiResource;
 
      $ResultArmado = (new $apiResource($instance))->response()->getData();
  
      return $this->dataResponse($ResultArmado, $code);

    }

    protected function showCollect(Collection $collection, $code = 200){
 
      if ($collection->isEmpty()) {
        return $this->dataResponse([], $code);
      }
      
      $apiResourceCollection = $collection->first()->apiResourceCollection;
          

      $collection = (new $apiResourceCollection($collection))->response()->getData();
      
  
      return $this->dataResponse($collection, $code);

    }

  #END PROTECTED 

}
